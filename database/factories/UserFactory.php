<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'username' => $faker->unique()->userName,
        'email_verified_at' => now(),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'username' => $faker->unique()->userName,
        'branch_id' => \App\Branch::inRandomOrder()->value('id'),
        'number'=> $faker->phoneNumber,
        'alternate_number'=> $faker->phoneNumber,
        'id_type'=> 1,
        'id_number'=> 123,
        'address'=> 'TamilNadu',
        'bank_name'=> 'HDFC',
        'bank_account_number'=> '0123456789',
        'bank_ifsc'=> '0147',
        'status'=> 1,
        'image'=> '',

    ];
});

$factory->define(App\Branch::class, function (Faker $faker) {
    return [
        'branch_name' => $faker->company,
        'branch_id' => $faker->randomNumber(),
        'branch_number' => $faker->phoneNumber,
        'branch_google_map' => $faker->userName,
        'branch_address' => $faker->address,
        'branch_country' => $faker->country,
        'branch_state' => $faker->state,
        'branch_city' => $faker->city,
        'branch_image' => '',
        'branch_manager_name' => $faker->name,
        'branch_manager_number' => $faker->phoneNumber,
        'printer_name' => 'POS-58-Series',
        'status' => 1,
    ];
});

$factory->define(App\Zone::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'branch_id' => \App\Branch::inRandomOrder()->value('id'),
        'status' => 1,
    ];
});

$factory->define(App\Kitchen::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'branch_id' => \App\Branch::inRandomOrder()->value('id'),
        'printer_name' => 'POS-58-Series',
        'status' => 1,
    ];
});

$factory->define(App\Table::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'zone_id' => \App\Zone::inRandomOrder()->value('id'),
        'table_capacity' => $faker->numberBetween(2, 8),
        'table_vendar' => '',
        'vendar_percentage' => 0,
        'status' => 1,
    ];
});

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'category' => $faker->name,
        'branch_id' => \App\Branch::inRandomOrder()->value('id'),
        'status' => 1,
    ];
});

$factory->define(App\Addon::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'price' => $faker->randomFloat(2, 0, 99),
        'kitchen_id' => \App\Kitchen::inRandomOrder()->value('id'),
        'status' => 1,
    ];
});

$factory->define(App\BillTitle::class, function (Faker $faker) {
    return [
        'title' => "M.M.A",
    ];
});

$factory->define(App\Menu::class, function (Faker $faker) {
    $tax = $faker->randomFloat(2, 0, 10);
    return [
        'menu_type' => 'food',
        'name' => $faker->name,
        'branch_id' => \App\Branch::inRandomOrder()->value('id'),
        'category_id' => \App\Category::inRandomOrder()->value('id'),
        'kitchen_id' => \App\Kitchen::inRandomOrder()->value('id'),
        'menu_desc' => $faker->realText(191, 2),
        'plate' => 0,
        'price' => $faker->randomFloat(2, 0, 99),
        'half_plate' => $faker->randomFloat(2, 0, 99),
        'familypack_plate' => $faker->randomFloat(2, 0, 99),
        'menu_item_spicy_level' => 'low',
        'menu_status' => 1,
        'menu_cuisine' => '',
        'menu_incl_excl' => 1,
        'menu_item_veg_nonveg' => 1,
        'cgst' => $tax,
        'sgst' => $tax,
        'short_code' => $faker->unique()->numberBetween(1, 100)
    ];
});

$factory->define(App\Supplier::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => 'Bangalore',
        'gst' => $faker->randomFloat(2, 0, 99),
        'number' => $faker->phoneNumber,
        'alternate_number' => $faker->phoneNumber,
        'contact_name' => $faker->name,
    ];
});