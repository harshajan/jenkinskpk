import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import API from '../services/ApiService'

const oldCart = {};
let oldKitchenOrders = {};

export default class Billing extends Component {
    constructor(){
        super();
        this.state = {
            zones: [],
            tables: [],
            categories: [],
            menus: [],
            activeZone: {},
            activeTable: {},
            selectedCategory: [],
            selectedMenu: [],
            selectedAddon: [],
            kitchenOrders: [],
            kitchenOrderItems: [],
            removedKitchenOrderItems: [],
            hasPendingKotOrders: false,
            checkingKotStatus: false,
            cartStatus: 'loading',
            billId: null,
        }
        this.handleZoneChange = this.handleZoneChange.bind(this);
        this.handleTableChange = this.handleTableChange.bind(this);
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
        this.handleMenuChange = this.handleMenuChange.bind(this);
        this.handleAddonChange = this.handleAddonChange.bind(this);
        this.handleKOTOrder = this.handleKOTOrder.bind(this);
        this.handlePrint = this.handlePrint.bind(this);
        this.handleAddItemById = this.handleAddItemById.bind(this);
        this.handleResetCart =  this.handleResetCart.bind(this);
    }
    async handleKOTOrder() {
        const data = {
            kot_orders: this.state.kitchenOrders.map(m => m.id),
            remove_kot_items: this.state.removedKitchenOrderItems,
            zone_id: this.state.activeZone.id,
            table_id: this.state.activeTable.id,
            menus: this.state.selectedMenu,
            addons: this.state.selectedAddon
        }
        this.setState({cartStatus: 'placingOrder'})
        API.placeOrder(data).then(res=> {
            this.updateKitchenOrders(res);
            this.setState({
                selectedMenu: [],
                selectedAddon: [],
                removedKitchenOrderItems: [],
                cartStatus: ''
            })
        })
    }
    async handlePrint() {
        this.setState({cartStatus: 'printing'})
        API.printBill(this.state.kitchenOrders.map(m => m.id), this.state.billId).then(res=> {
            this.setState({cartStatus: '', billId: res.id})
        })
    }
    async handleSettlement(type) {
        API.billPayment(this.state.billId, type).then(res=> {
            this.handleResetCart();
        })
    }
    async checkKOTStatus(){
        if (!this.state.checkingKotStatus) {
            const tableId = this.state.activeTable.id;
            this.setState({checkingKotStatus: true});
            API.checkKOTStatus(this.state.kitchenOrders.map(m => m.id)).then(res => {
                this.setState({checkingKotStatus: false});
                tableId === this.state.activeTable.id && this.updateKitchenOrders(res);
            });
        }
    }
    updateKitchenOrders(kitchenOrders) {
        const kitchenOrderItems = [];
        let hasPendingKotOrders = false;
        kitchenOrders.forEach(kOrder => {
            if(kOrder.kot_status !== 'accept'){
                hasPendingKotOrders = true;
            }
            kOrder.items.forEach(m => {
                m.price = Number(m.price);
                m.cgst = Number(m.cgst);
                m.sgst = Number(m.sgst);
                kitchenOrderItems.push(m)
            })
        });
        this.setState({
            kitchenOrders,
            kitchenOrderItems,
            billId: kitchenOrders.length && kitchenOrders[0].bill_id,
            hasPendingKotOrders
        });
        hasPendingKotOrders === true && setTimeout(this.checkKOTStatus.bind(this), 1000);
    }
    handleResetCart() {
        const categories = this.state.categories
        const selectedCategory = categories.length && [categories[0].id] || [];
        this.setState({
            selectedCategory,
            selectedMenu: [],
            selectedAddon: [],
            kitchenOrders: [],
            kitchenOrderItems: [],
            hasPendingKotOrders: false,
            cartStatus: '',
            billId: null
        })
    }
    handleZoneChange(event) {
        const activeZone = this.state.zones.find(z => z.id == event.target.value)
        this.setState({activeZone});
    }
    handleTableChange(table) {
        oldCart[this.state.activeTable.id] = {
            category: this.state.selectedCategory, 
            menus: this.state.selectedMenu, 
            addons: this.state.selectedAddon,
            kitchenOrders: this.state.kitchenOrders,
            kitchenOrderItems: this.state.kitchenOrderItems,
            removedKitchenOrderItems: this.state.removedKitchenOrderItems,
            hasPendingKotOrders: this.state.hasPendingKotOrders,  
            billId: this.state.billId
        };
        this.setState({activeTable: table});
        if(table.id in oldCart){
            this.setState({
                selectedCategory: oldCart[table.id].category,
                selectedMenu: oldCart[table.id].menus,
                selectedAddon: oldCart[table.id].addons,
                kitchenOrders: oldCart[table.id].kitchenOrders,
                kitchenOrderItems: oldCart[table.id].kitchenOrderItems,
                removedKitchenOrderItems: oldCart[table.id].removedKitchenOrderItems,
                hasPendingKotOrders: oldCart[table.id].hasPendingKotOrders,
                billId: oldCart[table.id].billId
            })
            oldCart[table.id].hasPendingKotOrders === true && setTimeout(this.checkKOTStatus.bind(this), 1000);
            delete oldCart[table.id]
        }
        else{
            this.handleResetCart()
            if(table.id in oldKitchenOrders){
                this.updateKitchenOrders(oldKitchenOrders[table.id])
                delete oldKitchenOrders[table.id]
            }
        }
    }
    handleCategoryChange(event) {
        const {selectedCategory, selectedMenu} = this.state
        const val = parseInt(event.target.value);
        if(event.target.checked){
            selectedCategory.push(val)
        }else{
            selectedCategory.splice(selectedCategory.indexOf(val), 1);
        }
        this.setState({selectedCategory: [val]});
    }
    handleMenuChange(menu) {
        let selectedMenu = this.state.selectedMenu
        if(event.target.checked){

            const price = menu.original_price * (1 + this.state.activeTable.vendar_percentage/100);
            selectedMenu.push({id: menu.id, quantity: 1, price, name: menu.name, kitchen_id: menu.kitchen_id, cgst: Number(menu.cgst), sgst: Number(menu.sgst)})
        }else{
            selectedMenu = selectedMenu.filter(a => a.id != menu.id);
        }
        this.setState({selectedMenu});
    }
    handleMenuCount(menu){
        let selectedMenu = this.state.selectedMenu
        if(event.target.value == 0) return false;
        selectedMenu[selectedMenu.indexOf(menu)].quantity = parseInt(event.target.value);
        this.setState({selectedMenu});
    }
    handleMenuRemove(menu) {
        let selectedMenu = this.state.selectedMenu
        selectedMenu.splice(selectedMenu.indexOf(menu), 1)
        this.setState({selectedMenu});
    }
    handleAddonChange(addon) {
        let selectedAddon = this.state.selectedAddon
        if(event.target.checked){
            const price = addon.original_price * (1 + this.state.activeTable.vendar_percentage/100);
            selectedAddon.push({id: addon.id, quantity: 1, price, name: addon.name, kitchen_id: addon.kitchen_id})
        }else{
            selectedAddon = selectedAddon.filter(a => a.id != addon.id);
        }
        this.setState({selectedAddon});
    }
    handleAddonCount(addon){
        let selectedAddon = this.state.selectedAddon
        if(event.target.value == 0) return false;
        selectedAddon[selectedAddon.indexOf(addon)].quantity = parseInt(event.target.value);
        this.setState({selectedAddon});
    }
    handleAddonRemove(addon) {
        let selectedAddon = this.state.selectedAddon
        selectedAddon.splice(selectedAddon.indexOf(addon), 1)
        this.setState({selectedAddon});
    }
    handleRemoveKitchenOrderItem(itemId) {
        const {removedKitchenOrderItems} = this.state;
        removedKitchenOrderItems.push(itemId);
        this.setState({removedKitchenOrderItems});
    }
    handleAddItemById(){
        const menuId = this.refs.menuItem.value;
        const menu = this.state.menus.find(m => m.short_code == menuId || m.name == menuId)
        if(!menu) {
            alert('Sorry, menu item not found')
            return false;
        }
        let selectedMenu = this.state.selectedMenu;
        const menuIndex = selectedMenu.findIndex(a => a.id == menu.id);
        if(menuIndex >= 0) {
            selectedMenu[menuIndex].quantity++;
        }else{
            const price = menu.original_price * (1 + this.state.activeTable.vendar_percentage/100);
            selectedMenu.push({id: menu.id, quantity: 1, price, name: menu.name, kitchen_id: menu.kitchen_id, cgst: Number(menu.cgst), sgst: Number(menu.sgst)})
        }
        this.setState({selectedMenu});
        this.refs.menuItem.value = '';
    }
    
    async componentDidMount() {
        const {zones, categories, menus, kitchenOrders} = await API.getZones();
        oldKitchenOrders = kitchenOrders;
        const activeZone = zones.length && zones[0] || {};
        const activeTable = activeZone && activeZone.tables && activeZone.tables[0] || {};
        
        this.setState({zones, categories, menus, activeZone: activeZone, cartStatus: ''})
        this.handleTableChange(activeTable)
    }
    renderBilling() {
        const zones = this.state.zones;
        const categories = this.state.categories;
        const menus = this.state.menus;
        let selectedMenuIds = this.state.selectedMenu.map(m => m.id)
        let selectedAddonIds = this.state.selectedAddon.map(m => m.id)
        const availableMenus = menus.filter(menu => this.state.selectedCategory.indexOf(menu.category_id) >= 0);
        let availableAddons = [], uniq = {}
        availableMenus.forEach(menu => {
            if (selectedMenuIds.indexOf(menu.id) >= 0) 
            availableAddons = availableAddons.concat(menu.addons)
        });
        availableAddons = availableAddons.filter(obj => !uniq[obj.id] && (uniq[obj.id] = true))
        const activeZone = this.state.activeZone;
        return (
            <div className="form-group" style={{width: '100%', display: 'flex'}}>
                <div className="col-md-2" style={{borderRight: '1px solid', borderColor: '#ddd'}}>
                    <div>
                        <h4>Zone</h4>
                        <hr/>
                        <select ref="table" onChange={this.handleZoneChange} value={activeZone.id} className="form-control">
                            {zones && zones.length && zones.map(zone => 
                                <option value={zone.id} key={zone.id}>{zone.name}</option>
                            )}
                        </select>
                    </div>

                    <div>
                    <br />
                        <h4>Table</h4>
                        <hr/>
                        <div className="form-group" style={{maxHeight: '300px', overflowY: 'auto'}}>
                            {activeZone.tables && activeZone.tables.map(table => 
                                <div className="funkyradio table_radio_size" key={table.id}>
                                    <input id={ 'table_' + table.id } type="radio" name="table" className="category_selected" onChange={() => this.handleTableChange(table)} checked={this.state.activeTable.id === table.id} value={table.id}></input>
                                    <label htmlFor={ 'table_' + table.id }>{ table.name }</label>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
                <div className="col-md-2" style={{borderRight: '1px solid', borderColor: '#ddd'}}>
                        <h4>Category</h4>
                        <hr />
                        <div className="row" style={{maxHeight: '420px', overflowY: 'auto'}}>
                            {categories && categories.map(category => 
                            <label className="container_radio" key={category.id} htmlFor={ 'category_' + category.id }>
                            { category.category }
                              <input id={ 'category_' + category.id } type="radio" name="category" onChange={ this.handleCategoryChange } checked={this.state.selectedCategory.indexOf(category.id) >= 0} value={category.id}></input>
                              <span className="checkmark_radio"></span>
                            </label>
                             
                        ) || 'No categories added'}
                        </div>
                </div>
                <div className="col-md-4" style={{borderRight: '1px solid', borderColor: '#ddd'}}>
                        <h4>Items</h4>
                        <hr/>
                        <div className="row" style={{maxHeight: '270px', overflowY: 'auto'}}>
                            {availableMenus.length != 0 &&availableMenus.map(menu => 
                                <div className="col-md-6" key={menu.id}>
                                <div className="form-group">
                                    <label className='container_c'>
                                    <input type="checkbox" value={menu.id} checked={selectedMenuIds.indexOf(menu.id) >= 0 } onChange={() => this.handleMenuChange(menu)}/>
                                    {menu.name} (#{menu.short_code})
                                    <span className="checkmark_c"></span>
                                    </label>
                                    </div>
                                </div>
                            ) || 'No menu available'}
                        </div>
                        <h4>Add-ons</h4>
                        <hr/>
                        <div className="row" style={{maxHeight: '135px', overflowY: 'auto'}}>
                            {availableAddons && availableAddons.map(addon => 
                            <div className="col-md-6" key={addon.id}>
                            <div className="form-group">
                                <label className='container_c'>
                                <input type="checkbox" value={addon.id} checked={selectedAddonIds.indexOf(addon.id) >= 0 } onChange={() => this.handleAddonChange(addon)}/>
                                {addon.name}
                                <span className="checkmark_c"></span>
                                </label>
                            </div>
                            </div>
                            )}
                        </div>
                </div>
                {this.renderCartList()}
            </div>
        );
    }
    renderKitchenOrders() {
        return (<tr></tr>)
    }
    renderCartList() {
        let subtotal = 0, cgst_total = 0, sgst_total = 0;
        return (<div className="col-md-4" style={{width: '36%'}}>
            <div className="row">
                <div className="col-sm-8" style={{paddingLeft: '0px'}}>
                    <input type="number" ref="menuItem" className="form-control1" placeholder="Enter Short Code" onKeyPress={(e) => e.key === 'Enter' && this.handleAddItemById()}></input>
                </div>
                <div className="col-sm-4">
                    <button className="btn btn-success" onClick={() => this.handleAddItemById()}>ADD</button>
                </div>
            </div>
            <table className="table table-bordered">
                <thead className="thead_bill">
                    <tr>
                        <th>Product</th>
                        <th>Rate</th>
                        <th>Quantity</th>
                        <th>Amount</th>
                        <th><i className="fa fa-folder-open" aria-hidden="true"></i></th>
                    </tr>
                </thead>
                <tbody className="tbody_bill">
                    { this.renderKitchenOrders() }
                {this.state.kitchenOrderItems && this.state.kitchenOrderItems.map(menu => {
                    const amount = menu.quantity * menu.price
                    subtotal += amount
                    cgst_total += menu.cgst * amount / 100;
                    sgst_total += menu.sgst * amount / 100;
                    return <tr key={menu.id} className={this.state.removedKitchenOrderItems.includes(menu.id) ? 'removed' : ''}>
                        <td>{ menu.name }</td>
                        <td>{ menu.price.toFixed(2) }</td>
                        <td><input type="number" value={ menu.quantity } readOnly={true} style={{width: '50px'}}/></td>
                        <td>{ amount.toFixed(2) }</td>
                        <td>
                            {this.state.removedKitchenOrderItems.includes(menu.id) || <i className="fa fa-trash font-black" aria-hidden="true" onClick={() => { this.handleRemoveKitchenOrderItem(menu.id)}}></i> }
                        </td>
                    </tr>
                }
                )}
                {this.state.selectedMenu && this.state.selectedMenu.map(menu => {
                    const amount = menu.quantity * menu.price;
                    subtotal += amount;
                    cgst_total += menu.cgst * amount / 100;
                    sgst_total += menu.sgst * amount / 100;
                    return <tr key={menu.id}>
                        <td>{ menu.name }</td>
                        <td>{ menu.price.toFixed(2) }</td>
                        <td><input type="number" value={ menu.quantity } onChange={() => this.handleMenuCount(menu)} style={{width: '50px'}}/></td>
                        <td>{ amount.toFixed(2) }</td>
                        <td><i className="fa fa-trash" aria-hidden="true" onClick={() => this.handleMenuRemove(menu)}></i></td>
                    </tr>
                }
                )}
                {this.state.selectedAddon && this.state.selectedAddon.map(addon => {
                    const amount = addon.quantity * addon.price;
                    subtotal += amount;
                    return <tr key={addon.id}>
                        <td>{ addon.name }</td>
                        <td>{ addon.price.toFixed(2) }</td>
                        <td><input type="number" value={ addon.quantity } onChange={() => this.handleAddonCount(addon)} style={{width: '50px'}}/></td>
                        <td>{ amount.toFixed(2) }</td>
                        <td><i className="fa fa-trash" aria-hidden="true" onClick={() => this.handleAddonRemove(addon)}></i></td>
                    </tr>
                }
                )}
                </tbody>
            </table>
            <div className="row">
                <div className="col-md-4">
                    <p>Sub Total</p>
                </div>
                <div className="col-md-8">
                    <input type="number" className="form-control" value={ subtotal.toFixed(2) } readOnly></input>
                </div>
                <div className="col-md-4">
                    <p>CGST</p>
                </div>
                <div className="col-md-8">
                    <input type="number" className="form-control" value={ cgst_total.toFixed(2) } readOnly></input>
                </div>
                <div className="col-md-4">
                    <p>SGST</p>
                </div>
                <div className="col-md-8">
                    <input type="number" className="form-control" value={ sgst_total.toFixed(2) } readOnly></input>
                </div>
                <div className="col-md-4">
                    <p>Grand Total</p>
                </div>
                <div className="col-md-8">
                    <input type="number" className="form-control" value={ (subtotal + cgst_total + sgst_total).toFixed(2) } readOnly></input>
                </div>
            </div>
            <div className="row">
                <button type="button" className="btn btn-primary" disabled={this.state.selectedMenu.length == 0 && this.state.removedKitchenOrderItems.length == 0} onClick={this.handleKOTOrder} style={{marginRight: '5px'}}>
                    {this.state.cartStatus == 'placingOrder' && <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>}
                    KOT
                </button>
                <button type="button" className="btn btn-warning" disabled={this.state.selectedMenu.length != 0 || this.state.kitchenOrders.length == 0 || this.state.removedKitchenOrderItems.length != 0 || this.state.hasPendingKotOrders === true} onClick={this.handlePrint} style={{marginRight: '5px'}}>
                    {this.state.cartStatus == 'printing' && <span className="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>}
                    {this.state.hasPendingKotOrders ? `Confirming` : 'Print'}
                </button>
                <div className="btn-group">
                    <button type="button" className="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" 
                        disabled={this.state.selectedMenu.length != 0 || this.state.kitchenOrders.length == 0 || this.state.removedKitchenOrderItems.length != 0 || this.state.hasPendingKotOrders === true || !this.state.billId} style={{marginRight: '5px'}}>
                        Settlement <span className="caret"></span>
                    </button>
                    <ul className="dropdown-menu">
                        <li><a className="pointer" onClick={() => this.handleSettlement('cash')}>By cash</a></li>
                        <li><a className="pointer" onClick={() => this.handleSettlement('card')}>By card</a></li>
                    </ul>
                </div>
                <button type="button" className="btn btn-danger" disabled={this.state.kitchenOrders.length != 0} onClick={this.handleResetCart}  style={{marginRight: '5px'}}>Clear</button>
            </div>
        </div>)
    }
    render() {
        return <div className="form-horizontal justify-content-center" style={{minHeight: '200px', 'display': 'flex'}}>
            {this.state.cartStatus == 'loading' ? 'Please wait' : this.renderBilling() }
        </div>
        
    }
}

if (document.getElementById('billing-order')) {
    ReactDOM.render(<Billing />, document.getElementById('billing-order'));
}
