<!DOCTYPE html>
<html>
<head>
	<title>KOT VIEW</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>
<link href="{{ asset('css/custom.css') }}" rel="stylesheet">
<!-- //SweetAlert -->

</head>
<body>
<div class="container-fluid">
	<div class="main-div">
		<div class="col-md-12">
			@if(count($kitchen_orders) > 0)
				@foreach($kitchen_orders->all() as $kitchen_order)
				<?php
		  			$status = $kitchen_order->kot_status;
		  			$datetime = $kitchen_order->created_at;
					$date = date('Y-m-d', strtotime($datetime));
					$time = date('H:i:s', strtotime($datetime));
		  		?>
				<div class="col-md-3 main_card" >
					<div class="card_casper">
						<!-- <i id = "x" class="fa fa-times-circle closeData" data-id="{{ $kitchen_order->id }}" id="{{ $kitchen_order->id }}" data-token="{{ csrf_token() }}" aria-hidden="true"></i> -->
						<!-- <button class="closeData x" data-id="{{ $kitchen_order->id }}" id="{{ $kitchen_order->id }}" data-token="{{ csrf_token() }}">X</button> -->
					  <div class="card-status" id="#card-status{{ $kitchen_order->id }}"
					  	<?php
					  		if ($status == 'accept') {
					  			echo 'style="background-color:green; color:#fff;"';
					  		}
					  		elseif($status == 'close'){
					  			echo 'style="background-color:red; color:#fff;"';
					  		}
					  		else{
					  			echo 'style="background-color:#ffeb3b;"';
					  		}
					  	?>

					  >
					  	<h3>KOT - {{ $kitchen_order->id }}</h3>
					  </div>
					  <div class="card-details">
					  	<h4><b>{{ $kitchen_order->table->name }} </b> <span class="pull-right"><?php echo $time;?></span></h4>
					  	

					  	 @foreach($kitchen_order_items as $kitchen_order_item)

					  	 <?php
						  	 $ko_id = $kitchen_order->id;
						  	 $koi_id = $kitchen_order_item->kitchen_order_id;
						  	 $item = $kitchen_order_item->name;
						  	 $quantity = $kitchen_order_item->quantity;

						  	 if ($ko_id == $koi_id) {
						  	 	echo $item." - ".$quantity."<br>";
						  	 }
					  	 ?>
					   
	                    @endforeach
					  	<?php
					  		if ($status == '') {
					  			?>
					  			<div class="card_control">
						  		<button class="btn btn-success btn-sm casper_btn acceptData" data-id="{{ $kitchen_order->id }}" id="{{ $kitchen_order->id }}" data-token="{{ csrf_token() }}">Accept</button> | 
						  	<button class="btn btn-danger btn-sm casper_btn rejectData" data-id="{{ $kitchen_order->id }}" id="{{ $kitchen_order->id }}" data-token="{{ csrf_token() }}">Reject</button>  	
						    </div>
					  			<?php
					  		}
					  	?>
					  	
					  </div>
					</div>
				</div>
			@endforeach
			@else
				<p> -- No KOT -- </p>
			@endif
		</div>
		<div class="rows">
			<p class="pull-right go_back"><a href="{{ url('/') }}">Go Back >></a></p>
		</div>
	</div>
</div>
<script type="text/javascript">
  /*main*/
  setInterval(function(){  minuteRefresh();}, 20 * 1000);
  function minuteRefresh(){
  	location.reload();
  };

$(document).ready(function(e){

    
	//Accept
	$(".acceptData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You Accept Order.!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-success",
		  confirmButtonText: "Yes, Accept it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "kot/accept/"+id,
		        type: 'post', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('#card-status'+id).css('background-color','green');
					$(el).closest('.card_control').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Accepted!", "Order Accepted", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your Order Not Accept :)", "error");
		  }
		});

	});
    //Reject
	$(".rejectData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You Close Order.!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, Close it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "kot/reject/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('.main_card').css('background','Red');
					$(el).closest('.main_card').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Rejected!", "Order Rejected", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	});

	//Accept
	$(".closeData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You Reject Order.!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-success",
		  confirmButtonText: "Yes, Reject it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "kot/close/"+id,
		        type: 'post', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('#card-status'+id).css('background-color','red');
					$(el).closest('.card_control').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Accepted!", "Order Closed", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your Order Not Closed :)", "error");
		  }
		});

	});   
	/**************************************************************************************************************/


});


</script>
</body>
</html>