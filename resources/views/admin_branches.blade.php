<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf_token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>

<!-- //SweetAlert -->
<!--//Metis Menu -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
.modal-content{
	border-radius: 0px;
}
.heading{
	margin-bottom: 2px;
}

</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4>Branches Details<label  class="pull-right" data-toggle="modal" data-target="#add_branch">Add <span data-toggle="tooltip" title="Add New" class="cursor_point"> <i class="fa fa-plus" aria-hidden="true" style="color: green;"></i></span></label> </h4>
				<div class="loading-overlay"><div class="overlay-content">Loading.....</div></div>
				<table class="table table-striped">
					<thead>
						<tr class="header">
							<th class="no-export">S.No</th>
							<th class="no-export">Image</th>
							<th>ID</th>
							<th>Branch</th>
							<th>Number</th>
							<th>Address</th>
							<th>Created</th>
							<th class="no-export">Status</th>
							<th class="no-export">Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($branches) > 0)
        					@foreach($branches->all() as $branch)
						<tr>
							<th></th>
							<th scope="row">
								@if ($branch->branch_image != '')
		        					<img src="images/branch_images/{{ $branch->branch_image }}" class="branch_img_size">
		        				@else
		        					<img src="images/demo_restaurant.png" class="staff_img_size">
		        				@endif
								
							</th>
							<th scope="row">{{ $branch->branch_id }}</th>
							<td>{{ $branch->branch_name }}</td>
							<td>{{ $branch->branch_number }}</td>
							<td>{{ $branch->branch_address }}</td>
							<td>{{ $branch->created_at->format('d/m/Y') }}</td>
							<td>
								@if($branch->status == 1)
								<i class="fa fa-check-circle-o" aria-hidden="true"></i>
								@else
								<i class="fa fa-times-circle-o" aria-hidden="true"></i>
								@endif
							</td>
							<td>
							<label data-toggle="modal" data-target="#view_branch{{ $branch->id }}"><span data-toggle="tooltip" class="cursor_point" title="View"><i class="fa fa-folder-open-o" aria-hidden="true"></i></span></label> | 
							<label data-toggle="modal" data-target="#edit_branch{{ $branch->id }}"><span data-toggle="tooltip" title="Edit" class="cursor_point"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></span></label> | 
							<label class="deleteData cursor_point" data-id="{{ $branch->id }}" id="{{ $branch->id }}" data-toggle="tooltip" title="Delete" data-token="{{ csrf_token() }}" ><i class="fa fa-trash"></i></label>
							</td>
						</tr>
						<!-- Start View Branch  -->
						  <div class="modal fade" id="view_branch{{ $branch->id }}" role="dialog">
						    <div class="modal-dialog modal-lg">
						    <!-- Modal content-->
						      <div class="modal-content">
						      	<form>
						        <div class="modal-header">
						          <button type="button" class="close" data-dismiss="modal">&times;</button>
						          <h4 class="modal-title">{{ $branch->branch_name }} Branch</h4>
						        </div>
						        <div class="modal-body">
						        	<div class="row">
						        		<div class="col-md-4">
											<center><img src="images/branch_images/{{ $branch->branch_image }}" class="branch_img_size"></center>
										</div>
										<div class="col-md-6">
											<div class="col-md-6 heading">
												<label class="label label-success"> ID </label>	
											</div>
											<div class="col-md-6 heading">
												<label> {{ $branch->branch_id }}</label>
											</div>
											<br>
											<div class="col-md-6 heading">
												<label class="label label-success"> Branch Name </label>	
											</div>
											<div class="col-md-6 heading">
												<label> {{ $branch->branch_name }}</label>
											</div>
											<br>
											<div class="col-md-6 heading">
												<label class="label label-success"> Number </label>	
											</div>
											<div class="col-md-6 heading">
												<label> {{ $branch->branch_number }}</label>
											</div>
											<br>
											<div class="col-md-6 heading">
												<label class="label label-success"> Address </label>	
											</div>
											<div class="col-md-6 heading">
												<label> {{ $branch->branch_address }}</label>
											</div>
										</div>
						        	</div>
						        	<div class="row">
						        		<h4>Manager Details</h4>
						        		<br>
						        		<div class="col-md-2">
											<label> Manager :</label>
										</div>
										<div class="col-md-4">
											<label> {{ $branch->branch_manager_name }}</label>
										</div>
										<div class="col-md-2">
											<label> Number</label>
										</div>
										<div class="col-md-4">
											<label> {{ $branch->branch_manager_number }}</label>
										</div>
						        	</div>
						        	
								</div>
						        <div class="modal-footer">
						         <!--  <button type="button" class="btn btn-success" data-dismiss="modal">Save</button>
						          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button> -->
						        </div>
						        </form>
						      </div>
						    </div>
						  </div>
						<!-- // End View Branch -->
						<!-- Start Edit Branch  -->
								  <div class="modal fade" id="edit_branch{{ $branch->id }}" role="dialog">
								    <div class="modal-dialog modal-lg">
								    <!-- Modal content-->
								      <div class="modal-content">
								      	<form  class="update_form" id="updateForm{{ $branch->id }}" data-toogle="validator">
								        <div class="modal-header">
								          <button type="button" class="close" data-dismiss="modal">&times;</button>
								          <h4 class="modal-title">Edit {{ $branch->branch_name }} Branch</h4>
								          <div class="loading-overlay"><div class="overlay-content">Loading.....</div></div>
								        </div>
								        <div class="modal-body">
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
														<label for="ID">ID</label>
														<input type="text" class="form-control" placeholder="Enter Branch ID" id="edit_branch_id" value="{{ $branch->branch_id }}" name="edit_branch_id" required="">
														<input type="hidden" class="form-control" name="branch_id" id="branch_id" value="{{ $branch->id }}">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Name">Name</label>
													    <input type="text" class="form-control" placeholder="Enter Branch Name" id="edit_branch_name" name="edit_branch_name" value="{{ $branch->branch_name }}" required="">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Number">Number</label>
													    <input type="text" class="form-control" id="edit_branch_number" name="edit_branch_number" placeholder="Enter Branch Number" value="{{ $branch->branch_number }}" required="">
													</div>
												</div>
								        	</div>
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Number">Manager Name</label>
													    <input type="text" class="form-control" id="edit_branch_manager_name" name="edit_branch_manager_name" placeholder="Enter Branch Manager Name" value="{{ $branch->branch_manager_name }}" required="">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Manager Number</label>
													    <input type="text" class="form-control" id="edit_branch_manager_number" name="edit_branch_manager_number" placeholder="Enter Branch Manager Number" value="{{ $branch->branch_manager_number }}" required="">
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Branch Google Maps Link</label>
													    <input type="text" class="form-control" id="edit_branch_google_map" name="edit_branch_google_map" placeholder="Enter Branch Map Link" value="{{ $branch->branch_google_map }}" required="">
													</div>
												</div>
								        	</div>
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Number">Country</label>
													    <select class="form-control" name="edit_branch_country" id="country_edit" required="">
													    	<option hidden="">--- Select Country ---</option>
													    	@if(count($countries) > 0)
									        					@foreach($countries->all() as $country)
														    		<option value="{{ $country->id }}" {{$country->id == "$branch->branch_country"  ? 'selected' : ''}}>{{ $country->name }}</option>
														    	@endforeach
										      				@else
										      				<option value="-"> -- No Data -- </option>
										      				@endif
													    </select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">State</label>
													    <select class="form-control" name="edit_branch_state" id="state_edit" required="">
													    	<option hidden="">--- Select State ---</option>
													    	@if(count($states) > 0)
									        					@foreach($states->all() as $state)
														    		<option value="{{ $state->id }}" {{$state->id == "$branch->branch_state"  ? 'selected' : ''}}>{{ $state->name }}</option>
														    	@endforeach
										      				@else
										      				<option value="-"> -- No Data -- </option>
										      				@endif
													    </select>
													</div>
												</div>
												<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">City</label>
													    <select class="form-control" name="edit_branch_city" id="city_edit" required="">
													    	<option hidden="">--- Select City ---</option>
													    	@if(count($cities) > 0)
									        					@foreach($cities->all() as $city)
														    		<option value="{{ $city->id }}" {{$city->id == "$branch->branch_city"  ? 'selected' : ''}}>{{ $city->name }}</option>
														    	@endforeach
										      				@else
										      				<option value="-"> -- No Data -- </option>
										      				@endif
													    </select>
													</div>
												</div>
								        	</div>
								        	<div class="row">
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Printer Name</label>
													    <input type="text" class="form-control" id="edit_printer_name" name="edit_printer_name" placeholder="Enter Printer Name" value="{{ $branch->printer_name }}" required="">
													</div>
													<div class="form-group">
														<label class="container_radio">Active
														  <input type="radio" name="edit_branch_status" value="1" {{ ($branch->status=="1")? "checked" : "" }}>
														  <span class="checkmark_radio"></span>
														</label>
														<label class="container_radio">Inactive
														  <input type="radio" name="edit_branch_status" value="0" {{ ($branch->status=="0")? "checked" : "" }}>
														  <span class="checkmark_radio"></span>
														</label>
													</div>
												</div>
								        		<div class="col-md-4">
													<div class="form-group">
													    <label for="Manager">Address</label>
													    <textarea class="form-control" name="edit_branch_address" id="edit_branch_address" rows="5" placeholder="Enter Branch Address" required="">{{ $branch->branch_address }}</textarea>
													</div>
												</div>

								        		<div class="col-md-4">
								        			<div class="form-group">
								        				<img src="images/branch_images/{{ $branch->branch_image }}" class="img-responsive">
										        		<label>Choose Image</label>
										        		<input type="file" name="edit_branch_image">
								        			</div>
								        		</div>
								        	</div>
								        	<div class="row">

								        	</div>
										</div>
								        <div class="modal-footer">
                          					<input type="hidden" value="{{$branch->id}}" class="branch_id"/>
								          <input type="button" name="submit" class="updateBtn btn btn-primary submitBtn" value="Update">
								          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
								        </div>
								        </form>
								      </div>
								    </div>
								  </div>
								<!-- // End Edit Branch -->
							@endforeach
	      				@endif
					</tbody>
				</table>
			<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
<!-- Form Start -->
<!-- Add Branch -->
  <div class="modal fade" id="add_branch" role="dialog">
    <div class="modal-dialog modal-lg">
    <!-- Modal content-->
      <div class="modal-content">
      	<form method="post" id="insert_form" enctype="multipart/form-data" data-toogle="validator">
          @csrf {{ method_field('POST') }}
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add Branch</h4>
        </div>
        <div class="modal-body">
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
						<label for="ID">ID</label>
						<input type="text" class="form-control" placeholder="Enter Branch ID" id="branch_id" name="branch_id" required="">
					</div>  			
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Name">Name</label>
					    <input type="text" class="form-control" placeholder="Enter Branch Name" id="branch_name" name="branch_name" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Number</label>
					    <input type="number" class="form-control" id="branch_number" name="branch_number" placeholder="Enter Branch Number" required="">
					</div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Manager Name</label>
					    <input type="text" class="form-control" id="branch_manager_name" name="branch_manager_name" placeholder="Enter Branch Manager Name" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Manager Number</label>
					    <input type="number" class="form-control" id="branch_manager_number" name="branch_manager_number" placeholder="Enter Branch Manager Number" required="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Branch Google Maps Link</label>
					    <input type="text" name="branch_google_map" id="branch_google_map" class="form-control" placeholder="Enter Branch Google Map Link">
					</div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Number">Country</label>
					    <select class="form-control" name="branch_country" id="country" required="">
					    	<option hidden="">--- Select Country ---</option>
					    	@if(count($countries) > 0)
	        					@foreach($countries->all() as $country)
						    		<option value="{{ $country->id }}">{{ $country->name }}</option>
						    	@endforeach
		      				@else
		      				<option value="-"> -- No Data -- </option>
		      				@endif
					    </select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">State</label>
					    <select name="branch_state" id="state" class="form-control" required="">
					    	<option value="-"> -- First Select Country -- </option>
                		</select>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">City</label>
					    <select name="branch_city" id="city" class="form-control" required="">
					    	<option value="-"> -- First Select State -- </option>
                		</select>
					</div>
				</div>
        	</div>
        	<div class="row">
        		<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Address</label>
					    <textarea class="form-control" required="" name="branch_address" id="branch_address" rows="5" placeholder="Enter Branch Address"></textarea>
					</div>
				</div>
				<div class="col-md-4">
					<div class="form-group">
					    <label for="Manager">Printer Name</label>
					    <input type="text" class="form-control" name="printer_name" id="printer_name" required="" placeholder="Enter Printer Name">
					</div>
				</div>
        		<div class="col-md-4">
        			<div class="form-group">
		        		<label>Choose Image</label>
		        		<input type="file" name="branch_image" id="branch_image" required="">
        			</div>	
        			<div class="form-group">
						<label class="container_radio">Active
						  <input type="radio" name="branch_status" value="1" checked="">
						  <span class="checkmark_radio"></span>
						</label>
						<label class="container_radio">Inactive
						  <input type="radio" name="branch_status" value="0">
						  <span class="checkmark_radio"></span>
						</label>
					</div>
        		</div>
        	</div>
        </div>
        <div class="modal-footer">
          <input type="submit" name="submit" class="btn btn-primary submitBtn" id="insertbutton" value="Save">
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
        </form>
      </div>
    </div>
  </div>
<!-- // End Branch -->

<!-- Form End -->
<!-- Footer Start -->
@include('inc.footer')
<!-- Footer End -->

</div>
<script type="text/javascript">
// Show loading overlay when ajax request starts
$( document ).ajaxStart(function() {
    $('.loading-overlay').show();
});
// Hide loading overlay when ajax request completes
$( document ).ajaxStop(function() {
    $('.loading-overlay').hide();
});
</script>
<!-- Script Started -->
<!-- <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> -->
<script type="text/javascript">
  /*main*/
  $(document).ready(function(e){
    $("#insert_form").on('submit', function(e){
      e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "{{ url('create_branch') }}",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
              $('#add_branch').modal('hide');
              $('#insert_form')[0].reset();
              window.location.href= 'admin_branches';
              swal("Good job!", "New Branch Created Successfully!", "success");

            },
            error : function(data){
              $('#add_branch').modal('hide');
              swal({
                title: 'Oops...',
                text: data.message,
                type: 'error',
                timer: '1500' 
              })
            }
        });
        return false;
    });

    $(".updateBtn").on('click', function(e){

      var id = $(this).parent().find('.branch_id').val();
      var form=$('#updateForm'+id)[0];
      var fd = new FormData(form);
		$.ajax({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
            type: 'POST',
            url: "branch/edit",
            data: fd,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('#edit_branch'+id).modal('hide');
              $('.update_form')[0].reset();
              window.location.href= 'admin_branches';
              swal("Good job!", "New Branch Created Successfully!", "success");
            },
            error : function(response){
              $('#edit_branch'+id).modal('hide');
              swal({
                title: 'Oops...',
                text: response.message,
                type: 'error',
                timer: '1500'
              })
            }
        });
    });


    //Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "You will not be able to recover this imaginary file!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, delete it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "branch/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Deleted!", "Your imaginary file has been deleted.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your imaginary file is safe :)", "error");
		  }
		});

	});   



/*dropdown*/
 $('#country').change(function(){
    var countryID = $(this).val();    
    if(countryID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-state-list')}}?country_id="+countryID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
                $("#state").empty();
                $("#state").append('<option>Select</option>');
                $.each(res,function(key,value){
                	$("#state").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
           
            }else{
               $("#state").empty();
            }
           }
        });
    }else{
        $("#state").empty();
        $("#city").empty();
    }      
   });

 	$('#state').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city").empty();
                $("#city").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#city").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
           
            }else{
               $("#city").empty();
            }
           }
        });
    }else{
        $("#city").empty();
    }
    });
/*// end*/
/* edit dropdown*/
 $('#country_edit').change(function(){
    var countryID = $(this).val();    
    if(countryID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-state-list')}}?country_id="+countryID,
           success:function(res){    
            if(res){
            	/*console.log(res);*/
                $("#state_edit").empty();
                $("#state_edit").append('<option>Select</option>');
                $.each(res,function(key,value){
                	$("#state_edit").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
           
            }else{
               $("#state_edit").empty();
            }
           }
        });
    }else{
        $("#state_edit").empty();
        $("#city_edit").empty();
    }      
   });

 	$('#state_edit').on('change',function(){
    var stateID = $(this).val();    
    if(stateID){
    	$.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
        $.ajax({
           type:"GET",
           url:"{{url('api/get-city-list')}}?state_id="+stateID,
           success:function(res){               
            if(res){
                $("#city_edit").empty();
                $("#city_edit").append('<option>Select</option>');
                $.each(res,function(key,value){
                    $("#city_edit").append('<option value="'+value['id']+'">'+value['name']+'</option>');
                });
           
            }else{
               $("#city_edit").empty();
            }
           }
        });
    }else{
        $("#city_edit").empty();
    }
    });
/*// end*/
});
</script>

<!-- End Script -->
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branches Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branches Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branches Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>