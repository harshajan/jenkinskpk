<!DOCTYPE html>
<html>
<head>
	<title>Hi</title>
<style>
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 8px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>
</head>
<body>
	<div class="container">
		<div>
			Table: {{ $bill->table->name }}
		</div>
	  <table class="table table-striped">
	    <thead>
	      <tr>
	        <th>Product</th>
	        <th>Rate</th>
	        <th>Quantity</th>
	        <th>Amount</th>
	      </tr>
	    </thead>
	    <tbody>
				@foreach($bill->items as $item)
	      <tr>
	        <td> {{ $item->name }} </td>
	        <td> {{ $item->price }} </td>
	        <td> {{ $item->quantity }} </td>
	        <td> {{ $item->total_amount }} </td>
	      </tr>
				@endforeach
	    </tbody>
	  </table>
		<div style="width: 50%; float: right">
			<div>
			Sub Total {{ $bill->sub_total }}
			</div>
			<div>
			CGST {{ $bill->cgst }} %
			</div>
			<div>
			SGST {{ $bill->sgst }} %
			</div>
			<div>
			Grand Total {{ $bill->bill_total }}
			</div>
		</div>
	</div>
</body>
</html>