<!-- 
COMPANY : CASPER TECHNOLOGY SERVICES PVT LTD
WEBSITE : www.casperindia.com
DEVELOPER : KALAISELVAN SANKAR
-->
<!DOCTYPE HTML>
<html>
<head>
<title>RESTAURANT</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<!-- <meta name="csrf_token" content="{{ csrf_token() }}"> -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<link href="data_tables/css/jquery.dataTables.css" rel='stylesheet' type='text/css' />
<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!-- Sweet alert -->
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript" src="js/sweetalert.js"></script>

<!-- //SweetAlert -->
<!--//Metis Menu -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
.dt-buttons{
		margin-bottom: 20px;
	}
.modal-content{
	border-radius: 0px;
}
.heading{
	margin-bottom: 2px;
}

</style>
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
@include('inc.header')
<!-- main content start-->
<div id="page-wrapper">
	<div class="main-page">
		<div class="tables">
			<div class="table-responsive bs-example widget-shadow">
				<h4>Branch Incoming Details</h4>
				<div class="loading-overlay"><div class="overlay-content">Loading.....</div></div>
				<table class="table table-striped">
					<thead>
						<tr>
							<th class="no-export">S.No</th>
							<th>Ingredient</th>
							<th>Quantity</th>
							<th>Created</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@if(count($branch_incomings) > 0)
        					@foreach($branch_incomings->all() as $branch_incoming)
						<tr>
							<th></th>
							<th scope="row">{{ $branch_incoming->ingredient->ingredient }}</th>
							<td>{{ $branch_incoming->quantity }}</td>
							<td>{{ $branch_incoming->created_at->format('d/m/Y') }}</td>
							<td>
							<label class="approvalData cursor_point btn btn-success btn-sm" data-id="{{ $branch_incoming->id }}" id="{{ $branch_incoming->id }}" data-toggle="tooltip" title="Approval" data-token="{{ csrf_token() }}" >Approval</label>
							 | 
							<label class="deleteData cursor_point btn btn-danger btn-sm" data-id="{{ $branch_incoming->id }}" id="{{ $branch_incoming->id }}" data-toggle="tooltip" title="Cancel" data-token="{{ csrf_token() }}" >Cancel</label>
							</td>
						</tr>
							@endforeach
	      				@endif
					</tbody>
				</table>
			<!-- Pagination --> 
			</div>
		</div>
	</div>
</div>
<!-- Form Start -->
<!-- Form End -->
<!-- Footer Start -->
@include('inc.footer')
<!-- Footer End -->
</div>

<!-- Script Start -->
<script type="text/javascript">
	//Delete
	
	$(".deleteData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "Stock Not Added",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-danger",
		  confirmButtonText: "Yes, Return it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "branch_incoming/delete/"+id,
		        type: 'delete', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Red');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Returned!", "Your Stock.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your Stock is safe :)", "error");
		  }
		});

	});
	//Approval

	$(".approvalData").click(function(e){
		e.preventDefault();
		var el = this;
		var id = this.id;
	swal({
		  title: "Are you sure?",
		  text: "New Stock Adding In Branch!",
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonClass: "btn-success",
		  confirmButtonText: "Yes, Approval it!",
		  cancelButtonText: "No, cancel plx!",
		  closeOnConfirm: false,
		  closeOnCancel: false
		},
		function(isConfirm) {
		  if (isConfirm) {
		  	
		    $.ajaxSetup({
		        headers: {
		            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		        }
		    });
		    $.ajax(
		    {
		        url: "branch_incoming/approval/"+id,
		        type: 'post', // replaced from put
		        dataType: "JSON",
		        data: {
		            "id": id // method and token not needed in data
		        },
		        success: function (response)
		        {
					/*alert(response["success"]);*/
		           $(el).closest('tr').css('background','Green');
					$(el).closest('tr').fadeOut(800, function(){ 
						$(this).remove();
					});	
		            
		            swal("Approval!", "Your Stock Added.", "success");
		            /*console.log(response); */// see the reponse sent
		        },
		        error: function(xhr) {
		         console.log(xhr.responseText); // this line will save you tons of hours while debugging
		         swal("Good job!", "You clicked the button!", "warning");
		        // do something here because of error
		       }
		    });
		    
		  } else {
		    swal("Cancelled", "Your Stock is safe :)", "error");
		  }
		});

	});   
</script>
<!-- End Script -->
<script type="text/javascript">
// Show loading overlay when ajax request starts
$( document ).ajaxStart(function() {
    $('.loading-overlay').show();
});
// Hide loading overlay when ajax request completes
$( document ).ajaxStop(function() {
    $('.loading-overlay').hide();
});
</script>
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
	<script src="js/bootstrap.js"> </script>
	<!-- Data Tables -->
	<script src="data_tables/js/jquery.dataTables.js"></script>
	<script src="data_tables/js/dataTables.buttons.min.js"></script>
	<script src="data_tables/js/jszip.min.js"></script>
	<script src="data_tables/js/pdfmake.min.js"></script>
	<script src="data_tables/js/vfs_fonts.js"></script>
	<script src="data_tables/js/buttons.html5.min.js"></script>
	<script src="data_tables/js/buttons.print.min.js"></script>

	
	<script type="text/javascript">
		$(document).ready(function(){
			var table=$(".table").DataTable({
				dom: 'Blfrtip',
				lengthMenu:[
					[10,25,50,-1],
					["10","25","50","all"]
				],
				
       		buttons: [
       		{
       			extend: 'excel',
       			text: 'Excel',
       			className: 'btn btn-success',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branch Incoming Details"
       		},
       		{
       			extend: 'pdf',
       			text: 'PDF',
       			className: 'btn btn-danger',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branch Incoming Details"
       		},
       		{
       			extend: 'print',
       			text: 'Print',
       			className: 'btn btn-warning',
       			exportOptions:{
       				columns: ':not(.no-export)'
       			},
       			title: "Branch Incoming Details"
       		}
       		]
			});
			table.on('order.dt search.dt', function(){
				table.column(0,{search: 'applied',order: 'applied'}).nodes().each(function(cell, index){
					cell.innerHTML=index+1;
				});
			}).draw();
		});
	</script>
	<script>
		$(document).ready(function(){
		    $('[data-toggle="tooltip"]').tooltip();   
		});
	</script>
	<!-- End -->
</body>
</html>