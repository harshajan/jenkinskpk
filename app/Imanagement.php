<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Imanagement extends Model
{
    protected $fillable = [
      'id', 'name', 'unit', 'quantity', 'category'
   ];
}
