<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = [
        'id', 'menu_type', 'branch_id', 'category_id', 'menu_desc', 'name', 'plate', 'price', 'half_plate', 'familypack_plate', 'menu_cuisine', 'menu_incl_excl', 'menu_tax', 'menu_addons', 'menu_item_spicy_level', 'menu_item_desc', 'kitchen_id', 'menu_item_veg_nonveg', 'menu_status', 'cgst', 'sgst', 'short_code'
    ];

    protected $appends = ['original_price', 'net_price'];

    public function branch(){
      return $this->belongsTo('App\Branch');
    }

    public function category(){
    	
      return $this->belongsTo('App\Category');
    }

    public function addons() {
      return $this->belongsToMany('App\Addon','menu_addons');
    }

    public function taxes() {
      return $this->belongsToMany('App\Tax');
    }

    public function recipe()
    {
        return $this->hasMany('\App\Recipe');
    }

    public function getoriginalPriceAttribute(){
      if($this->attributes['menu_incl_excl']){
        return $this->attributes['price'] / ($this->attributes['sgst'] + $this->attributes['sgst'] + 100) * 100;
      }
      return $this->attributes['price'];
    }
    public function getcgstPriceAttribute(){
      return $this->attributes['cgst'] * $this->getoriginalPriceAttribute() / 100;
    }
    public function getsgstPriceAttribute(){
      return $this->attributes['sgst'] * $this->getoriginalPriceAttribute() / 100;
    }
    public function getnetPriceAttribute(){
      return $this->getoriginalPriceAttribute() + $this->getcgstPriceAttribute() + $this->getsgstPriceAttribute();
    }
}