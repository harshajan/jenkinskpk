<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Incoming;
use App\Observers\IncomingObserver;
use App\Outgoing;
use App\Observers\OutgoingObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Illuminate\Support\Facades\Schema::defaultStringLength(191);
        Incoming::observe(IncomingObserver::class);
        Outgoing::observe(OutgoingObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
