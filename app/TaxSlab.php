<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TaxSlab extends Model
{
    protected $fillable = [
        'id', 'tax_slab', 'status'
    ];
}
