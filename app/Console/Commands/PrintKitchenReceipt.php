<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mike42\Escpos\Printer;
use Mike42\Escpos\PrintConnectors\UriPrintConnector;
use App\KitchenOrder;
use App\Kitchen;

class PrintKitchenReceipt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'print:kitchen {bill : The ID of the bill}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $billId = $this->argument('bill');
        $bill = KitchenOrder::findOrFail($billId);
        $branchItems = $bill->items->groupBy('kitchen_id');
        $branchItems->each(function($items, $kitchen_id) use($bill) {
            $this->printKitchenItems(Kitchen::find($kitchen_id), $bill, $items);
        });
        
    }

    public function printKitchenItems($kitchen, $bill, $items)
    {
        try {
            $connector = UriPrintConnector::get($kitchen->printer_name);
            $printer = new Printer($connector);
            // /* Initialize */
            $printer->initialize();
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $printer->feed(1);
            $printer->text("--------------------------------\n");
            $printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
            $printer->text("KOT : ".sprintf('%05d', $bill->id)."\nTBL : {$bill->table->name}\n");

            $printer->text("\n");
            $printer -> selectPrintMode();
            $printer->text("--------------------------------\n");
            
            $printer->setJustification(Printer::JUSTIFY_LEFT);
            $items->each(function($item) use($printer) {
                $printer->text("{$item->quantity}     $item->name\n");
            });
            $printer->text("--------------------------------\n");
            $printer->text("--------------------------------\n");
            $printer->feed(3);
            $printer->cut();
            $printer->close();
        } catch (Exception $e) {
            echo "Couldn't print to this printer: " . $e -> getMessage() . "\n";
        }
    }
}
