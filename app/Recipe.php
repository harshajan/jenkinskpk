<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    protected $fillable = [
        'id', 'recipe_id', 'menu_category_id', 'menu_id', 'ingredient_id', 'category_id','quantity', 'unit_id'
    ];

    public function ingredient(){
      return $this->belongsTo('App\Ingredient');
    }

    public function category(){
      return $this->belongsTo('App\IngredientCategory');
    }

    public function unit(){
      return $this->belongsTo('App\IngredientUnit');
    }

    public function menu_category(){
      return $this->belongsTo('App\Category');
    }

    public function menu(){
      return $this->belongsTo('App\Menu');
    }
}
