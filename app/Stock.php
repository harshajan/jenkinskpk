<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    protected $fillable = ['branch_id', 'ingredient_id', 'quantity'];

    public function ingredient(){
      return $this->belongsTo('App\Ingredient');
    }
}
