<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tax;
class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'category'=>$request['category'],
            'percentage'=>$request['percentage'],
            'status'=>$request['status']
        ];
        return Tax::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $tax_id = $request['tax_id'];
        $tax_update=Tax::where('id', $tax_id)->first();

     $data_updated=[
         'category'=>$request['edit_category'],
         'percentage'=>$request['edit_percentage'],
         'status'=>$request['edit_status']

     ];
     $tax_update->update($data_updated);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Tax::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllTax()
    {
        $taxes = Tax::orderBy('id', 'desc')->get();
        return view('admin_tax',['taxes'=>$taxes]);

    }
}
