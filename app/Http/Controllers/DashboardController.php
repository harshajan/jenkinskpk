<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bill;
use App\Table;
use App\Zone;
use App\Branch;
use App\User;
class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function AllDashboard()
    {
        $Bill_total = Bill::sum('net_total');//Auth::user()->products->sum('price');
        $branches = Branch::count();
        
        $this->middleware(['permission:Manage zones|Manage branch zones']);
        $zones = Zone::when(auth()->user()->hasPermissionTo('Manage branch zones'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->latest('id')->get();
        $this->middleware(['permission:Manage tables|Manage branch tables']);
        $tables = Table::when(auth()->user()->hasPermissionTo('Manage branch tables'), function($query){
            $query->whereHas('zone', function($query){
                $query->where('branch_id', auth()->user()->branch_id);
            });
        })->latest('id')->get();
        $this->middleware(['permission:Manage users|Manage branch users']);
        $users = User::when(auth()->user()->hasPermissionTo('Manage branch users'), function($query){
            $query->whereHas('user', function($query){
                $query->where('branch_id', auth()->user()->branch_id);
            });
        })->latest('id')->count();
        
        return view('admin',['Bill_total'=>$Bill_total])
                    ->with("branches",$branches)
                    ->with("zones",$zones)
                    ->with("tables",$tables)
                    ->with("users",$users);
        /*$data = Bill::sum('net_total');
        print_r($data);*/
    }
} 
