<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use App\Country;
use App\State;
use App\City;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $img_id = $request['branch_id'];
            if( $request->hasFile('branch_image')) {
                $image = $request->file('branch_image');
                $path = public_path(). '/images/branch_images/';
                $RandomImageNumber = uniqid();
                $filename = $img_id . '_' . time() . $RandomImageNumber . '.' . $image->getClientOriginalExtension();
                $image->move($path, $filename);

                
            }

        $data=[
            'branch_id'=>$request['branch_id'],
            'branch_name'=>$request['branch_name'],
            'branch_number'=>$request['branch_number'],
            'branch_google_map'=>$request['branch_google_map'],
            'branch_address'=>$request['branch_address'],
            'branch_country'=>$request['branch_country'],
            'branch_state'=>$request['branch_state'],
            'branch_city'=>$request['branch_city'],
            'branch_image'=>$filename,
            'branch_manager_name'=>$request['branch_manager_name'],
            'branch_manager_number'=>$request['branch_manager_number'],
            'printer_name'=>$request['printer_name'],
            'status'=>$request['branch_status']
        ];
        return Branch::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
 {
   $branch_id = $request['branch_id'];
   $branch=Branch::where('id', $branch_id)->first();
   $image_name=$branch->branch_image;

     
         if( $request->hasFile('edit_branch_image')) {
           if($image_name)
           {
             unlink(public_path(). '/images/branch_images/'.$image_name);
           }
             $image = $request->file('edit_branch_image');
             $path = public_path(). '/images/branch_images/';
             $RandomImageNumber = uniqid();
             $filename = time() . $RandomImageNumber . '.' . $image->getClientOriginalExtension();
             $image->move($path, $filename);
             $image_name=$filename;
         }

     $data=[
         'branch_id'=>$request['edit_branch_id'],
         'branch_name'=>$request['edit_branch_name'],
         'branch_number'=>$request['edit_branch_number'],
         'branch_google_map'=>$request['edit_branch_google_map'],
         'branch_address'=>$request['edit_branch_address'],
         'branch_country'=>$request['edit_branch_country'],
         'branch_state'=>$request['edit_branch_state'],
         'branch_city'=>$request['edit_branch_city'],
         'branch_image'=>$image_name,
         'branch_manager_name'=>$request['edit_branch_manager_name'],
         'branch_manager_number'=>$request['edit_branch_manager_number'],
         'printer_name'=>$request['edit_printer_name'],
         'status'=>$request['edit_branch_status']
     ];
     $branch->update($data);

     return response()->json(['success' => 'Record has been Updated successfully!']);

 }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Branch::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);

    }

    // read all data for branches
    public function AllBranch()
    {
        $branches = Branch::orderBy('id', 'desc')->get();
        $countries = Country::all();
        $states = State::all();
        $cities = City::all();
        return view('admin_branches',['branches'=>$branches])
                ->with("countries",$countries)
                ->with("states",$states)
                ->with("cities",$cities);

    }

}
