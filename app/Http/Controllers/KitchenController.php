<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kitchen;
use App\KitchenOrder;
use App\Branch;
use App\User;
use App\DB;
use App\KitchenOrderItem;
class KitchenController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(empty( $_POST['chef'])){
            $chef_list = '0';

        }else{
            $chef_list = implode(',', $_POST['chef']);
        }
    
        
        $data=[
            'name'=>$request['kitchen_name'],
            'branch_id'=>$request['kitchen_branch'],
            'printer_name'=>$request['printer_name'],
            'chef'=>$chef_list,
            'status'=>$request['kitchen_status']
        ];
        return Kitchen::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $kitchen_id = $request['kitchen_id'];
        
        if(empty( $_POST['edit_chef'])){
            $chef_list = '0';

        }else{
            $chef_list = implode(',', $_POST['edit_chef']);
        }
        
        
        $kitchen_update=Kitchen::where('id', $kitchen_id)->first();

     $data=[
         'name'=>$request['edit_kitchen_name'],
         'branch_id'=>$request['edit_kitchen_branch'],
         'printer_name'=>$request['printer_name'],
         'chef'=>$chef_list,
         'status'=>$request['edit_kitchen_status']
     ];
     $kitchen_update->update($data);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Kitchen::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllKitchen()
    {
        $this->middleware(['permission:Manage kitchens|Manage branch kitchens']);
        $kitchen = Kitchen::when(auth()->user()->hasPermissionTo('Manage branch kitchens'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->latest('id')->get();
        $branches = Branch::when(auth()->user()->hasPermissionTo('Manage branch kitchens'), function($query){
            $query->where('id', auth()->user()->branch_id);
        })->where('status', 1)->get();
        $users = User::when(auth()->user()->hasPermissionTo('Manage branch users'), function($query){
            $query->where('role', 'Chef');
        })->where('role', 'Chef')->get();
        return view('admin_kitchen',['kitchens'=>$kitchen])->with("branches",$branches)->with("users",$users);

    }

    public function Kot()
    {
        $kitchen_orders = KitchenOrder::when(auth()->user()->hasPermissionTo('Manage branch kot'), function($query){
            $query->where('branch_id', auth()->user()->branch_id)->whereNull('kot_status')->orWhere('kot_status', 'accept');
        })->get();

        $kitchen_order_items = KitchenOrderItem::all();

        return view('kot_view',['kitchen_orders'=>$kitchen_orders])
                ->with('kitchen_order_items', $kitchen_order_items);


    }

    public function KotRejected($id)
    {

        KitchenOrder::destroy($id);
        return response()->json([
            'success' => 'Record has been Rejected successfully!'
        ]);
    }

    public function KotAccept($id)
    {

    $order_accept=KitchenOrder::where('id', $id)->first();

     $data=[
         'kot_status'=>'accept'
     ];

     $order_accept->update($data);

     return response()->json([
            'success' => 'Record has been Accepted successfully!'
        ]);

    }

    public function KotClose($id)
    {

    $order_close=KitchenOrder::where('id', $id)->first();

     $data=[
         'kot_status'=>'close'
     ];

     $order_close->update($data);

     return response()->json([
            'success' => 'Record has been Closed successfully!'
        ]);

    }

    public function getChefList(Request $request)
    {
        $branch_id = $request['branch_id'];
        $chefs=User::where('branch_id', $branch_id)->where('role', 'Chef')->get();

        return response()->json($chefs);
    }

}
