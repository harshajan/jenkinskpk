<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Zone;
use App\Table;
use App\Category;
use App\Menu;
use App\Addon;
use App\Bill;
use App\KitchenOrder;
use App\KitchenOrderItem;
use PDF;
use App\Recipe;
use App\Stock;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branch = auth()->user()->branch;
        $res['zones'] = $branch->zones()->where('status', 1)->with(['tables' => function ($query) {
            $query->where('status', 1);
        }])->get();
        $res['categories'] = $branch->categories()->where('status', '=', 1)->get();
        $res['menus'] = Menu::where('menu_status', '=', 1)->with(['addons' => function ($query) {
            $query->where('status', '=', 1);
        }])->where('branch_id', $branch->id)->get();
        $res['kitchenOrders'] = $branch->kitchenOrders()->where(function ($query) {
            $query->whereNull('bill_id')->orWhereHas('bill', function ($query) {
                $query->whereNull('payment_type');
            });
        })->with('items')->get()->groupBy('table_id');
        return $res;
    }
    public function placeOrder()
    {
        $kotOrderIds = request()->input('kot_orders', []);
        $removeKotItems = request()->input('remove_kot_items', []);
        if (count($removeKotItems)) {
            $kotItemsAll = KitchenOrderItem::whereIn('id', $removeKotItems)->get()->groupBy(['kitchen_order_id', 'kitchen_id']);
            $kotOrderIdsToUpdate = KitchenOrderItem::whereIn('id', $removeKotItems)->pluck('kitchen_order_id');

            //  \Artisan::call('print:kitchenCancelled', ['itemIds' => implode($removeKotItems, ',')]);

            KitchenOrderItem::whereIn('id', $removeKotItems)->delete();
        }
        if (count(request()->input('menus')) || count(request()->input('addons'))) {
            $bill = request()->only(['zone_id', 'table_id']);
            $lastInvoiceId = KitchenOrder::latest()->value('invoice_no');
            if (!$lastInvoiceId) {
                $nextInvoiceNumber = date('Ymd') . '-00001';
            } else {
                $expNum = explode('-', $lastInvoiceId);
                if ($expNum[0] == date('Ymd'))
                    $nextInvoiceNumber = "{$expNum[0]}-" . sprintf('%05d', (int) $expNum[1] + 1);
                else
                    $nextInvoiceNumber = date('Ymd') . '-00001';
            }
            $bill['branch_id'] = auth()->user()->branch_id;
            $bill['invoice_no'] = $nextInvoiceNumber;
            $bill['cgst'] = $bill['sgst'] = $bill['sub_total'] = $bill['net_total'] = 0;
            $billItems = [];
            $quantity = 0;
            foreach (request()->input('menus') as $menu) {
                $billItem = [
                    'itemable_id' => $menu['id'],
                    'itemable_type' => 'menu',
                    'kitchen_id' => $menu['kitchen_id'],
                    'name' => $menu['name'],
                    'quantity' => $menu['quantity'],
                    'price' => $menu['price'],
                    'cgst' => $menu['cgst'],
                    'sgst' => $menu['sgst'],
                ];

                $amount = $billItem['price'] * $billItem['quantity'];
                $cgst_amount = $amount * $billItem['cgst'] / 100;
                $sgst_amount = $amount * $billItem['sgst'] / 100;

                $billItem['total_amount'] = $amount + $cgst_amount + $sgst_amount;
                $billItems[] = $billItem;

                $quantity += $menu['quantity'];

                $bill['cgst'] += $cgst_amount;
                $bill['sgst'] += $sgst_amount;
                $bill['sub_total'] += $amount;
                $bill['net_total'] += $billItem['total_amount'];
            }
            foreach (request()->input('addons') as $addon) {
                $billItem = [
                    'itemable_id' => $addon['id'],
                    'itemable_type' => 'addon',
                    'kitchen_id' => $addon['kitchen_id'],
                    'name' => $addon['name'],
                    'quantity' => $addon['quantity'],
                    'price' => $addon['price'],
                    'cgst' => isset($addon['cgst']) ? $addon['cgst'] : 0,
                    'sgst' => isset($addon['sgst']) ? $addon['sgst'] : 0
                ];
                $amount = $billItem['price'] * $billItem['quantity'];
                $cgst_amount = $amount * $billItem['cgst'] / 100;
                $sgst_amount = $amount * $billItem['sgst'] / 100;

                $billItem['total_amount'] = $amount + $cgst_amount + $sgst_amount;
                $billItems[] = $billItem;

                $quantity += $addon['quantity'];

                $bill['cgst'] += $cgst_amount;
                $bill['sgst'] += $sgst_amount;
                $bill['sub_total'] += $amount;
                $bill['net_total'] += $billItem['total_amount'];
            }
            $bill['quantity'] = $quantity;
            $bill['status'] = 'placed';
            $bill = auth()->user()->kitchenOrders()->create($bill);
            $bill->items()->createMany($billItems);
            $kotOrderIds[] = $bill->id;
            // \Artisan::call('print:kitchen', ['bill' => $bill->id ]);
        }
        return KitchenOrder::with('items')->find($kotOrderIds);


        return $bill->load('items');
    }

    public function printOrder()
    {
        $kitchenOrderIds = request()->input('order');
        if (request()->has('bill') && request()->input('bill')) {
            $bill = Bill::find(request()->input('bill'));
            $bill->items()->delete();
        } else {
            $bill = \App\KitchenOrder::select('zone_id', 'table_id', 'branch_id')->find($kitchenOrderIds[0])->toArray();
            $bill = new Bill($bill);
            $lastInvoiceId = Bill::latest()->value('invoice_no');
            if (!$lastInvoiceId) {
                $nextInvoiceNumber = date('Ymd') . '-00001';
            } else {
                $expNum = explode('-', $lastInvoiceId);
                if ($expNum[0] == date('Ymd'))
                    $nextInvoiceNumber = "{$expNum[0]}-" . sprintf('%05d', (int) $expNum[1] + 1);
                else
                    $nextInvoiceNumber = date('Ymd') . '-00001';
            }
            $bill['invoice_no'] = $nextInvoiceNumber;
            $bill['user_id'] = auth()->id();
        }
        $bill['cgst'] = $bill['sgst'] = $bill['sub_total'] = $bill['net_total'] = 0;
        $billItems = [];
        $quantity = 0;
        $kitchenItems = \App\KitchenOrderItem::whereIn('kitchen_order_id', $kitchenOrderIds)
            ->select(\DB::raw('sum(quantity) as quantity'), 'itemable_id', 'itemable_type')
            ->groupBy('itemable_id', 'itemable_type')
            ->get()->toArray();
        foreach ($kitchenItems as $kitchenItem) {
            if ($kitchenItem['itemable_type'] == 'menu') {
                $item = Menu::find($kitchenItem['itemable_id']);
            } else {
                $item = Addon::find($kitchenItem['itemable_id']);
            }
            $price = $item['original_price'] * (1 + $bill->table->vendar_percentage / 100);
            $billItem = [
                'itemable_id' => $item['id'],
                'itemable_type' => $kitchenItem['itemable_type'],
                'kitchen_id' => $item['kitchen_id'],
                'name' => $item['name'],
                'quantity' => $kitchenItem['quantity'],
                'price' => $price,
                'cgst' => isset($item['cgst']) ? $item['cgst'] : 0,
                'sgst' => isset($item['sgst']) ? $item['sgst'] : 0
            ];
            $amount = $billItem['price'] * $billItem['quantity'];
            $cgst_amount = $amount * $billItem['cgst'] / 100;
            $sgst_amount = $amount * $billItem['sgst'] / 100;

            $billItem['total_amount'] = $amount + $cgst_amount + $sgst_amount;
            $billItems[] = $billItem;

            $quantity += $kitchenItem['quantity'];

            $bill['cgst'] += $cgst_amount;
            $bill['sgst'] += $sgst_amount;
            $bill['sub_total'] += $amount;
            $bill['net_total'] += $billItem['total_amount'];
        }
        $bill['quantity'] = $quantity;
        $bill['status'] = 'placed';
        $bill->save();
        $bill->items()->createMany($billItems);
        \App\KitchenOrder::whereIn('id', $kitchenOrderIds)->update(['bill_id' => $bill->id]);

        // \Artisan::call('print:receipt', ['bill' => $bill->id ]);

        return $bill;
    }

    public function updatePayment(Bill $bill)
    {
        $menuIds = $bill->items()->where(['itemable_type' => 'menu'])
            ->pluck('itemable_id')->toArray();
        Recipe::whereIn('menu_id', $menuIds)
            ->select(\DB::raw('sum(quantity) as quantity'), 'ingredient_id')
            ->groupBy('ingredient_id')
            ->get()
            ->each(function ($recipeItem) use ($bill) {
                Stock::where([
                    'ingredient_id' => $recipeItem->ingredient_id,
                    'branch_id' => $bill->branch_id
                ])
                    ->decrement('quantity', $recipeItem->quantity);
            });
        $bill->update(request()->only(['payment_type']));
        return $bill;
    }

    public function pendingOrders()
    {
        return auth()->user()->branch->bills()
            ->with(['table', 'items'])
            ->latest('id')
            ->limit(100)
            ->get();
    }

    public function checkKotStatus()
    {
        $kotOrderIds = request()->input('kot_orders', []);
        return KitchenOrder::with('items')->find($kotOrderIds);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // read all data for branches
    public function AllBill()
    {
        $zones = Zone::where('status', '=', 1)->get();
        $tables = Table::where('status', '=', 1)->get();
        $categories = Category::where('status', '=', 1)->get();
        $items = Menu::where('menu_status', '=', 1)->get();
        $addons = Addon::where('status', '=', 1)->get();
        return view('billing', ['zones' => $zones])
            ->with("tables", $tables)
            ->with("categories", $categories)
            ->with("items", $items)
            ->with("addons", $addons);
    }

    public function getTableList(Request $request)
    {
        $bill_zone_id = $request['bill_zone_id'];
        $tables = Table::where('zone_id', $bill_zone_id)->get();

        return response()->json($tables);
    }

    public function getItemList(Request $request)
    {
        $category_id = $request['category_id'];
        $tables = Menu::where('category_id', $category_id)->get();

        return response()->json($tables);
    }

    public function getItemSelected(Request $request)
    {
        $addons = Addon::whereHas('menus', function ($query) {
            $query->whereIn('menus.id', request()->input('item_ids'));
        })->get();
        return response()->json($addons);
    }

    public function generatePDF()
    {
        $data = ['title' => 'Welcome to HDTuto.com'];
        $pdf = PDF::loadView('myPDF', $data);

        //return $pdf->download('itsolutionstuff.pdf');
        return $pdf->stream('invoice.pdf');
    }

    public function print()
    {
        \Artisan::call('print:receipt', ['bill' => 2]);
    }
}
