<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Zone;
use App\Branch;
class ZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data=[
            'name'=>$request['zone_name'],
            'branch_id'=>$request['zone_branch'],
            'status'=>$request['zone_status']
        ];
        return Zone::create($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $zone_id = $request['zone_id'];
        $zone_update=Zone::where('id', $zone_id)->first();

     $data=[
         'name'=>$request['edit_zone_name'],
         'branch_id'=>$request['edit_zone_branch'],
         'status'=>$request['edit_zone_status']
     ];
     $zone_update->update($data);

     return response()->json(['success' => 'Record has been Updated successfully!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Zone::destroy($id);
        return response()->json([
            'success' => 'Record has been deleted successfully!'
        ]);
    }

    public function AllZone()
    {
        $this->middleware(['permission:Manage zones|Manage branch zones']);
        $zones = Zone::when(auth()->user()->hasPermissionTo('Manage branch zones'), function($query){
            $query->where('branch_id', auth()->user()->branch_id);
        })->latest('id')->get();
        $branches = Branch::when(auth()->user()->hasPermissionTo('Manage branch zones'), function($query){
            $query->where('id', auth()->user()->branch_id);
        })->where('status', 1)->get();
        return view('admin_zone',['zones'=>$zones])->with("branches",$branches);

    }
}
