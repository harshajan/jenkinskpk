<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = [
        'id', 'name', 'table_capacity', 'branch_id', 'zone_id', 'table_vendar', 'vendar_percentage', 'status'
    ];

    public function zone(){
      return $this->belongsTo('App\Zone');
  }
}
